package pw.hais.bankcalling.dao;

import java.util.HashMap;
import java.util.Map;

import android_serialport_api.CHexConver;
import pw.hais.bankcalling.utils.CallUtil;

/**
 * Created  on 2016/8/21/021.
 */

public class LEDDao {
    private static byte identify = 0x7E; //标识
    private static byte agreement = (byte) 0xE9; //协议
    private static Map<Integer,String> oldMap = new HashMap<>();

    /**
     * 发送排队信息给客户端 D(0x44)+显示区域(0x01)+文字
     */
    public static void sendMessageToLed(int call, String content) {
        try {
            if(content.equals(oldMap.get(call)))return;
            int[] wait = new int[]{0x44,0x01};   //等待人数
            wait = CallUtil.arraycat(wait,bytesToInts(content.getBytes("GB2312")));   //等待人数
            int[] agreements = CallUtil.arraycat(new int[]{agreement}, wait);                 //拼接协议
            int[] base = CallUtil.arraycat(new int[]{call}, agreements);     //拼接地址
            CallDao.postMsgToSerialport(base);
            oldMap.put(call,content);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 发送评价
     * @param call
     * @param status 1 发送欢迎，2评价
     */
    public static void sendMessageToPingjia(int call, int status) {
        try {
            int[] wait = new int[0];
            if (status == 1) {
                wait = new int[]{0x44,0x01,0x59};   //发送欢迎语
            }else if(status == 2){
                wait = new int[]{0x44,0x01,0x5A};   //发送欢迎语
            }
            int[] agreements = CallUtil.arraycat(new int[]{0xE5}, wait);                 //拼接协议
            int[] base = CallUtil.arraycat(new int[]{call}, agreements);     //拼接地址
            CallDao.postMsgToSerialport(base);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static int[] bytesToInts(byte[] bytes){
        int[] ints = new int[bytes.length];
        for (int i=0;i<bytes.length;i++){
            ints[i] = bytes[i];
        }
        return ints;
    }

}
