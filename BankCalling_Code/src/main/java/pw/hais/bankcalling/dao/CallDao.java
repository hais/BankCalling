package pw.hais.bankcalling.dao;

import java.util.List;

import android_serialport_api.CHexConver;
import pw.hais.bankcalling.app.HaisApp;
import pw.hais.bankcalling.entity.Counter;
import pw.hais.bankcalling.entity.greendao.BusinessDao;
import pw.hais.bankcalling.utils.BusinessUtil;
import pw.hais.bankcalling.utils.CallUtil;
import pw.hais.utils.L;

/**
 * 呼叫器
 * Created  on 2016/7/25/025.
 */
public class CallDao {
    private static byte identify = 0x7E; //标识
    private static byte agreement = (byte) 0xE5; //协议

    public static void sendWaitNumber(List<Counter> counterses) {
        try {
            int waitNumber = HaisApp.daoSession.getBusinessDao().queryBuilder().where(BusinessDao.Properties.Status.eq(1)).build().list().size();
            for (Counter c : counterses) {
                int[] wait = CHexConver.hexStr2Bytes(CHexConver.str2HexStr("R" + BusinessUtil.getNumber(waitNumber, 3)));   //等待人数
                int[] agreements = CallUtil.arraycat(new int[]{agreement}, wait);                 //拼接协议
                int[] base = CallUtil.arraycat(CHexConver.hexStr2Bytes(BusinessUtil.getNumber(Integer.parseInt(c.call), 2)), agreements);     //拼接地址
                postMsgToSerialport(base);
                Thread.sleep(100);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 发送排队信息给客户端 W+nnnnn+www
     */
    public static void sendNumberClient(int call, String schar, int no) {
        try {
            int[] wait = CHexConver.hexStr2Bytes(CHexConver.str2HexStr("W" + BusinessUtil.getNumberCode(schar, no)));   //等待人数
            int[] agreements = CallUtil.arraycat(new int[]{agreement}, wait);                 //拼接协议
            int[] base = CallUtil.arraycat(new int[]{call}, agreements);     //拼接地址
            postMsgToSerialport(base);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 给客户端发送派号确认 E+nnnnn
     */
    public static void replyNumberToClient(int call) {
        try {
            int[] wait = CHexConver.hexStr2Bytes(CHexConver.str2HexStr("EP"));   //等待人数
            int[] agreements = CallUtil.arraycat(new int[]{agreement}, wait);                 //拼接协议
            int[] base = CallUtil.arraycat(new int[]{call}, agreements);     //拼接地址
            postMsgToSerialport(base);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 给客户端发送长鸣 F+n   n=20
     */
    public static void sendAudio(int call, int len) {
        try {
            int[] wait = CHexConver.hexStr2Bytes(CHexConver.str2HexStr("F"));   //头
            wait = CallUtil.arraycat(wait, new int[]{len});   //长度
            int[] agreements = CallUtil.arraycat(new int[]{agreement}, wait);                 //拼接协议
            int[] base = CallUtil.arraycat(new int[]{call}, agreements);     //拼接地址
            postMsgToSerialport(base);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 给客户端发送登陆确认 Q
     */
    public static void replyLoginToClient(int call, int[] baseDate) {
        try {
            int[] wait = CHexConver.hexStr2Bytes(CHexConver.str2HexStr("EQ"));   //头
            wait = CallUtil.arraycat(wait, baseDate);                 //拼接协议
            int[] agreements = CallUtil.arraycat(new int[]{agreement}, wait);                 //拼接协议
            int[] base = CallUtil.arraycat(new int[]{call}, agreements);     //拼接地址
            postMsgToSerialport(base);
            Thread.sleep(100);
            if (baseDate == null) CallDao.sendAudio(call, 20);
            else CallDao.sendAudio(call, 125);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 加密处理
     *
     * @param base
     */
    public static void postMsgToSerialport(int[] base) {
        try {

            int[] bytes = new int[]{};
            bytes = CallUtil.arraycat(bytes, base);
            int crc = (CallUtil.createCRC(base, base.length) & 0x0000ffff);
            bytes = CallUtil.arraycat(bytes, new int[]{BusinessUtil.getLow4(crc), BusinessUtil.getHeight4(crc)});    //取高低位
            bytes = BusinessUtil.escape(bytes); //转义
            bytes = CallUtil.arraycat(bytes, new int[]{identify}); //标识
            bytes = CallUtil.arraycat(new int[]{identify}, bytes); //标识
            if(HaisApp.callUtil!=null)HaisApp.callUtil.pushMsg(bytes);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
