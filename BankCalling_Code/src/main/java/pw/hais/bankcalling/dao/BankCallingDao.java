package pw.hais.bankcalling.dao;

import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import pw.hais.app.UtilConfig;
import pw.hais.bankcalling.app.HaisApp;
import pw.hais.bankcalling.app.V_HTTP;
import pw.hais.bankcalling.entity.Business;
import pw.hais.bankcalling.entity.Machine;
import pw.hais.bankcalling.model.BaseModel;
import pw.hais.bankcalling.model.CallingModel;
import pw.hais.bankcalling.model.RegisterModel;
import pw.hais.bankcalling.utils.BusinessUtil;
import pw.hais.http.Http;
import pw.hais.http.base.OnHttpListener;
import pw.hais.utils.L;
import pw.hais.utils.TimeFormatUtil;

/**
 * 优云后台
 * Created  on 2016/7/24/024.
 */
public class BankCallingDao extends BaseDao {

    //设备注册
    public static void machineRegister(Machine machine, OnHttpListener<RegisterModel> listener) {
        Map<String, Object> data = getBaseDate();
        data.put("mac", machine.mac);                   //网卡地址
        data.put("ip", machine.ip);                     //设备IP地址
        data.put("native_code", machine.native_code);   //机器唯一码
        data.put("dev_name", machine.dev_name);         //设备名称
        data.put("dev_no", machine.dev_no);             //设备编号
        data.put("dev_type", machine.dev_type);         //设备类型(Android排队终端)
        Http.post(V_HTTP.REGISTER_MACHINE, null, null, new JSONObject(data), listener);
    }

    //获取初始化数据
    public static void getServerInitDate(OnHttpListener<CallingModel> listener) {
        Map<String, Object> data = getBaseDate();
        Http.post(V_HTTP.INIT_MACHINE, null, null, new JSONObject(data), listener);
    }


    //上传分数
    public static void updateDate(Business b, OnHttpListener<BaseModel> listener) {
        List<Business> waits = new ArrayList<>();
        for(Business business:DateCacheDao.getBusinesses()){
            business.setNo(DateCacheDao.findWaitNumber(business.getSchar()));
            waits.add(business);
        }

        Map<String, Object> data = getBaseDate();
        data.put("serial_no", b.getSchar()+ HaisApp.machine.channel_no+BusinessUtil.getNumber(b.getNo(),4));    //业务前缀符号+渠道编码+年+月+日+四位自增变量
        data.put("number", BusinessUtil.getNumberCode(b.getSchar(), b.getNo()));                                //排队号码(规则:一位前缀符+四位阿拉伯数字)
        data.put("name", b.getName());                                                                         //业务名称
        data.put("sid", b.getSid());                                                                            //业务编码
        data.put("state", b.getStatus() + "");                                                                  //号码状态(0:已经放弃、1：已经取票、2:已经叫号、3：开始服务、4：办理完成)
        data.put("ticket_time", b.getTicket_time());                              //开始取票时间
        data.put("call_time", b.getCall_time());                                  //开始呼叫时间
        data.put("saccept_time", b.getSaccept_time());                            //开始办理时间
        data.put("eaccept_time", b.getEaccept_time());                            //受理结束时间
        data.put("mark", b.getMark() + "");                                                                     //评价键值结果	(评价键值(1:非常满意、2：满意、3：一般、4：差))
        data.put("content", b.getCall() + "");                                                                  //窗口
        data.put("empno", b.getStaffNo() + "");                                                                 //员工编号
        data.put("total_num", DateCacheDao.findAllWaitNumber() + "");                                           //总等待人数
        data.put("accept_num", DateCacheDao.findAllNumber() + "");                                              //受理总人数
        data.put("waits",waits);                                                                               //各业务等待人数

        try {
            Http.post(V_HTTP.UPLOAD_MACHINE, null, null, new JSONObject(UtilConfig.GSON.toJson(data)), listener);
        }catch (Exception e){e.printStackTrace();}
    }
}
