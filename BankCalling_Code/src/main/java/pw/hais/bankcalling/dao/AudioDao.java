package pw.hais.bankcalling.dao;

import android.content.Context;

import com.baidu.tts.auth.AuthInfo;
import com.baidu.tts.client.SpeechError;
import com.baidu.tts.client.SpeechSynthesizer;
import com.baidu.tts.client.SpeechSynthesizerListener;
import com.baidu.tts.client.TtsMode;

import pw.hais.bankcalling.app.V;
import pw.hais.utils.L;
import pw.hais.utils.ResourceUtil;

/**
 * 语音播放控制
 * Created  on 2016/8/4/004.
 */
public class AudioDao implements SpeechSynthesizerListener {
    private SpeechSynthesizer speechSynthesizer;



    public AudioDao(Context context){
        // 获取 tts 实例
        speechSynthesizer = SpeechSynthesizer.getInstance();
        // 设置 app 上下文（必需参数）
        speechSynthesizer.setContext(context);
        // 设置 tts 监听器
        speechSynthesizer.setSpeechSynthesizerListener(this);
        // 文本模型文件路径，文件的绝对路径 (离线引擎使用)
        speechSynthesizer.setParam(SpeechSynthesizer.PARAM_TTS_TEXT_MODEL_FILE, ResourceUtil.geFileFromAssets(context,"bd_etts_ch_text.dat"));
        // 声学模型文件路径，文件的绝对路径 (离线引擎使用)
        speechSynthesizer.setParam(SpeechSynthesizer.PARAM_TTS_SPEECH_MODEL_FILE, ResourceUtil.geFileFromAssets(context,"bd_etts_ch_speech_male.dat"));
        speechSynthesizer.setParam(SpeechSynthesizer.PARAM_VOLUME, "5");    //中级音量，范围[0-9]
        speechSynthesizer.setParam(SpeechSynthesizer.PARAM_SPEED, "5");     //中速，范围[0-9]
        speechSynthesizer.setParam(SpeechSynthesizer.PARAM_PITCH, "5");     //中调，范围[0-9]
        // 本地授权文件路径 , 如未设置将使用默认路径 . 设置临时授权文件路径，
//        LICENCE_FILE_NAME 请替换成临时授权文件的实际路径，仅在使用临时 license 文件时需要进行设置，如果在[应用管理]中开通了离线授权，不需要设置该参数，建议将该行代码删除（离线引擎）
//        speechSynthesizer.setParam(SpeechSynthesizer.PARAM_TTS_LICENCE_FILE, LICENSE_FILE_FULL_PATH_NAME);
        // 请替换为语音开发者平台上注册应用得到的 App ID (离线授权)
        speechSynthesizer.setAppId(V.BAIDU_TTS_APPID);
        // 请替换为语音开发者平台注册应用得到的 apikey 和 secretkey (在线授权)
        speechSynthesizer.setApiKey(V.BAIDU_TTS_APIKEY, V.BAIDU_TTS_SECRTKEY);
        // 授权检测接口
        AuthInfo authInfo = speechSynthesizer.auth(TtsMode.MIX);
        if (authInfo.isSuccess()) {
            L.i("AudioDao","语音授权成功！");
        }else{
            L.i("AudioDao","语音授权失败！");
        }
            // 引擎初始化接口
        speechSynthesizer.initTts(TtsMode.MIX);

    }


    public void speech(String text,String textId){
        speechSynthesizer.speak(text,textId);
        L.i("AudioDao","播放语音-"+textId+"："+text);
    }









    @Override
    public void onSynthesizeStart(String s) {

    }

    @Override
    public void onSynthesizeDataArrived(String s, byte[] bytes, int i) {

    }

    @Override
    public void onSynthesizeFinish(String s) {

    }

    @Override
    public void onSpeechStart(String s) {

    }

    @Override
    public void onSpeechProgressChanged(String s, int i) {

    }

    @Override
    public void onSpeechFinish(String s) {

    }

    @Override
    public void onError(String s, SpeechError speechError) {

    }
}
