package pw.hais.bankcalling.dao;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import pw.hais.bankcalling.app.HaisApp;
import pw.hais.bankcalling.app.V;
import pw.hais.utils.RandomUtil;
import pw.hais.utils.SHAUtil;

/**
 * Created  on 2016/7/25/025.
 */
public class BaseDao {

    public static Map<String, Object> getBaseDate() {
        String timestamp = new Date().getTime() + "";               //时间戳
        String nonce = RandomUtil.getRandomNumbers(8);              //8位數字随机数
        String signature = SHAUtil.getSHA(V.KAIYOUYUN_KEY + timestamp + nonce);     //签名

        Map<String, Object> data = new HashMap<>();
        data.put("channel_no", HaisApp.machine.channel_no);     //渠道编码(机构编码)
        data.put("nonce", nonce);                               //随机数
        data.put("appkey", HaisApp.machine.appkey);                    //appkey
        data.put("signature", signature);                      //加密签名
        data.put("timestamp", timestamp);                       //时间戳
        return data;
    }
}
