package pw.hais.bankcalling.dao;

import org.greenrobot.greendao.query.QueryBuilder;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import pw.hais.bankcalling.app.HaisApp;
import pw.hais.bankcalling.entity.Business;
import pw.hais.bankcalling.entity.BusinessWeight;
import pw.hais.bankcalling.entity.Counter;
import pw.hais.bankcalling.entity.Staff;
import pw.hais.bankcalling.entity.greendao.BusinessDao;
import pw.hais.bankcalling.model.CallingModel;
import pw.hais.utils.EmptyUtil;
import pw.hais.utils.SPUtil;

import static pw.hais.bankcalling.app.V.SP_COUNTERS_LIST;

/**
 * 本地数据处理
 * Created  on 2016/8/4/004.
 */
public class DateCacheDao {
    private static List<Business> baseBusinesses = new ArrayList<>();        //Base所有业务
    private static List<Business> businesses = new ArrayList<>();            //所有业务,去除二级分层
    private static List<Counter> counters = new ArrayList<>();                  //所有柜台
    private static Map<String, List<Counter>> sidByCounterMap = new HashMap<>();   //sid，获取权重序列
    private static Map<String, List<Business>> conByBusinessMap = new HashMap<>();   //con，获取权重序列
    private static Map<String, Staff> onByStaffsMap = new HashMap<>();     //on，获取权重序列

    //缓存所有数据
    public static void saveCallingInfo(CallingModel.Calling calling) {
        //所有业务。
        baseBusinesses = calling.business;
        for (Business business : calling.business) {
            if (EmptyUtil.emptyOfList(business.children)) {
                businesses.add(business);
            } else {
                for (Business b : business.children) {
                    businesses.add(b);
                }
            }
        }

        //所有柜台,根据call排序,从小到大
        counters = calling.counters;
        Collections.sort(counters, new Comparator<Counter>() {
            public int compare(Counter c0, Counter c1) {
                int call0 = Integer.parseInt(c0.call);
                int call1 = Integer.parseInt(c1.call);
                if (call0 > call1) {
                    return 1;
                } else if (call0 == call1) {
                    return 0;
                } else {
                    return -1;
                }
            }
        });
        SPUtil.saveObject(SP_COUNTERS_LIST, counters);


        //根据业务 查询优先级。
        for (Business business : businesses) {  //循环业务
            List<Counter> counterList = new ArrayList<>();
            for (Counter counter : counters) {  //循环窗口
                for (BusinessWeight weight : counter.regulation) { //循环权重
                    if (business.getSid().equals(weight.sid)) {    //如果
                        counter.diyWeight = weight.weight;
                        if (counterList.isEmpty()) {
                            counterList.add(counter);
                        } else {
                            for(int i=0;i<counterList.size();i++){
                                if (weight.weight < counterList.get(i).diyWeight) {
                                    counterList.add(i == 0 ? 0 : i - 1, counter);   //插入
                                }
                                break;
                            }
                        }
                        break;
                    }
                }
            }
            sidByCounterMap.put(business.getSid(), counterList);
        }

        //根据地址，排序优先级
        for (Counter counter : calling.counters) {
            List<Business> businessList = new ArrayList<>();
            for (BusinessWeight weight : counter.regulation) { //循环权重
                for (Business b : businesses) {
                    if (weight.sid.equals(b.getSid())) {
                        businessList.add(b);
                        break;
                    }
                }
            }
            conByBusinessMap.put(counter.cno, businessList);
        }

        //根据员工ID，排序员工信息
        for (Staff staff : calling.staffs) {
            onByStaffsMap.put(staff.no, staff);
        }

    }

    /**
     * 员工登陆到 柜台
     *
     * @param call    柜台地址
     * @param staffNo 空为退出登陆
     * @return
     */
    public static boolean staffLogin(int call, String staffNo) {
        call = call - 1;
        //退出登陆
        if (EmptyUtil.emptyOfString(staffNo)) {
            counters.get(call).staff = null;
            return false;
        }
        //登陆
        counters.get(call).staff = onByStaffsMap.get(staffNo);
        if (counters.get(call).staff == null) return false;
        else return true;
    }

    /**
     * 根据 窗口地址查询业务
     *
     * @param call
     */
    public static Business findBusinessByCon(int call, int status) {
        List<Business> BusinnessList = conByBusinessMap.get(String.valueOf(call));
        if (EmptyUtil.emptyOfList(BusinnessList)) return null;
        for (Business business : BusinnessList) {
            QueryBuilder<Business> queryBuilder = HaisApp.daoSession.getBusinessDao().queryBuilder();
            queryBuilder.where(BusinessDao.Properties.Sid.eq(business.getSid()), BusinessDao.Properties.Status.eq(status));
            queryBuilder.orderAsc(BusinessDao.Properties.No);
//            queryBuilder.limit(1);
            List<Business> list = queryBuilder.build().list();
//            L.e("查询：" + UtilConfig.GSON.toJson(list));
            if (!EmptyUtil.emptyOfList(list)) return list.get(0);
        }
        return null;
    }

    /**
     * 根据业务代号 获取排序数量
     *
     * @param schar
     * @return
     */
    public static int findNewBusinessNoBySchar(String schar) {
        QueryBuilder<Business> queryBuilder = HaisApp.daoSession.getBusinessDao().queryBuilder();
        queryBuilder.where(BusinessDao.Properties.Schar.eq(schar));
        queryBuilder.orderDesc(BusinessDao.Properties.No);
        queryBuilder.limit(1);
        List<Business> business = queryBuilder.build().list();
        if (EmptyUtil.emptyOfList(business)) return 0;
        else return business.get(0).getNo();
    }

    /**
     * 根据业务代号获取等待人数
     *
     * @param schar
     * @return
     */
    public static int findWaitNumber(String schar) {
        QueryBuilder queryBuilder = HaisApp.daoSession.getBusinessDao().queryBuilder();
        queryBuilder.where(BusinessDao.Properties.Schar.eq(schar), BusinessDao.Properties.Status.eq(1));
        return (int) queryBuilder.count();
    }

    /**
     * 获取等待总人数
     *
     * @return
     */
    public static int findAllWaitNumber() {
        QueryBuilder queryBuilder = HaisApp.daoSession.getBusinessDao().queryBuilder();
        queryBuilder.where(BusinessDao.Properties.Status.eq(1));
        return (int) queryBuilder.count();
    }

    /**
     * 获取等待总人数
     *
     * @return
     */
    public static int findAllNumber() {
        QueryBuilder queryBuilder = HaisApp.daoSession.getBusinessDao().queryBuilder();
        return (int) queryBuilder.count();
    }


    public static List<Business> getBusinesses() {
        return businesses;
    }

    public static List<Business> getBaseBusinesses() {
        return baseBusinesses;
    }

    public static List<Counter> getCounters() {
        return counters;
    }

    public static Map<String, List<Counter>> getSidByCounterMap() {
        return sidByCounterMap;
    }

    public static String getCounterListBySid(String sid) {
        StringBuffer sb = new StringBuffer();
        List<Counter> list = sidByCounterMap.get(sid);
        for (Counter c : list) {
            sb.append(c.cno + "、");
        }
        if(sb.length()!=0)sb.delete(sb.length()-1,sb.length());
        return sb.toString();
    }

    public static Map<String, List<Business>> getConByBusinessMap() {
        return conByBusinessMap;
    }

}
