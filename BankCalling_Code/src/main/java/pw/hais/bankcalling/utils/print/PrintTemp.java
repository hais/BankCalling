package pw.hais.bankcalling.utils.print;

import java.util.Date;

import android_serialport_api.CHexConver;
import pw.hais.bankcalling.dao.DateCacheDao;
import pw.hais.bankcalling.entity.Business;
import pw.hais.bankcalling.entity.BusinessWeight;
import pw.hais.bankcalling.entity.Counter;
import pw.hais.bankcalling.utils.BusinessUtil;
import pw.hais.utils.EmptyUtil;
import pw.hais.utils.TimeFormatUtil;

/**
 * Created  on 2016/8/1/001.
 */
public class PrintTemp {
    //打印排队小票
    public static void printMessage(final Printer printer, final Business business, String printTmple) {
//        printer.printByte(CHexConver.hexStr2Byte("1B 40 1D 21 66 1B 61 01 BB FA B9 B9 BA C5 1B 61 00 1D 21 00 0A 1D 21 66 1B 61 01 50 30 30 30 31 1B 61 00 1D 21 00 0A 1D 21 33 1B 61 01 B8 F6 C8 CB D2 B5 CE F1 1B 61 00 1D 21 00 0A 33 A1 A2 31 20 B4 B0 BF DA D3 D0 20 31 20 C8 CB D4 DA B5 C8 B4 FD 0A 1D 21 11 1B 61 01 BB B6 D3 AD B9 E2 C1 D9 1B 61 00 1D 21 00 1B 61 02 0A 32 30 31 36 2D 31 31 2D 31 35 20 30 39 3A 31 39 3A 35 30 0A 1B 64 04 1B 6D 10 04 04"));
//        if(1==1)return;
//        printTmple = "<H1><center>标题</center></H1><br><br>你好：<br><H5><center>正文...</center></H5><br><br>";
//        printTmple = "<H1><center>机构号</center></H1><br><H1><center>排队号</center></H1><br><H4><center>业务名</center></H4><br>窗口号 窗口有 等待数 人在等待<br><H6><center>欢迎光临</center></H6>";
//        printTmple = "<H1></H1><br><br>NiHao:<br><H5></H5><br><br>";

        printTmple = labelToString(printer.getInit()) + printTmple;      //添加初始化

        printTmple = printTmple.replace("<H1>", labelToString(printer.getBig_x(0x66)));        //处理标题 打开
        printTmple = printTmple.replace("<H2>", labelToString(printer.getBig_x(0x55)));        //处理标题 打开
        printTmple = printTmple.replace("<H3>", labelToString(printer.getBig_x(0x44)));        //处理标题 打开
        printTmple = printTmple.replace("<H4>", labelToString(printer.getBig_x(0x33)));        //处理标题 打开
        printTmple = printTmple.replace("<H5>", labelToString(printer.getBig_x(0x22)));        //处理标题 打开
        printTmple = printTmple.replace("<H6>", labelToString(printer.getBig_x(0x11)));        //处理标题 打开
        printTmple = printTmple.replace("<H7>", labelToString(printer.getBig_x(0x00)));        //处理标题 打开
        printTmple = printTmple.replace("</H1>", labelToString(printer.getBig_x(0)));      //处理标题 关闭
        printTmple = printTmple.replace("</H2>", labelToString(printer.getBig_x(0)));      //处理标题 关闭
        printTmple = printTmple.replace("</H3>", labelToString(printer.getBig_x(0)));      //处理标题 关闭
        printTmple = printTmple.replace("</H4>", labelToString(printer.getBig_x(0)));      //处理标题 关闭
        printTmple = printTmple.replace("</H5>", labelToString(printer.getBig_x(0)));      //处理标题 关闭
        printTmple = printTmple.replace("</H6>", labelToString(printer.getBig_x(0)));      //处理标题 关闭
        printTmple = printTmple.replace("</H7>", labelToString(printer.getBig_x(0)));      //处理标题 关闭

//        printTmple = printTmple.replace("<b>", labelToString(printer.getBeiKuan()));        //处理加粗 打开
//        printTmple = printTmple.replace("</b>", labelToString(printer.getBeiKuanClose()));       //处理加粗 关闭
//
//        printTmple = printTmple.replace("<i>", labelToString(printer.getTITLE_COLSE()));        //处理斜体 打开
//        printTmple = printTmple.replace("</i>", labelToString(printer.getTITLE_COLSE()));       //处理斜体 关闭

        printTmple = printTmple.replace("<center>", labelToString(printer.getCenter()));    //处理居中 打开
        printTmple = printTmple.replace("</center>", labelToString(printer.getLeft()));    //处理居中 关闭

        printTmple = printTmple.replace("<br>", labelToString(printer.getBr()));          //处理换行

        printTmple = printTmple.replace("业务名", business.getName());         //处理业务名称
        printTmple = printTmple.replace("排队号", BusinessUtil.getNumberCode(business.getSchar(), business.getNo()));         //处理排队号
        printTmple = printTmple.replace("等待数", DateCacheDao.findWaitNumber(business.getSchar()) + "");         //处理等待数
        printTmple = printTmple.replace("窗口号", DateCacheDao.getCounterListBySid(business.getSid()));         //处理优先级
        printTmple = printTmple + labelToString(printer.getRight());    //靠右
        printTmple = printTmple + "\n" + TimeFormatUtil.getTimeFormat(new Date().getTime() + "", "yyyy-MM-dd HH:mm:ss") + "\n";   //添加时间
        printTmple = printTmple + labelToString(printer.getPaper(6));    //走纸
        printTmple = printTmple + labelToString(printer.getCutting());    //切纸
        printTmple = printTmple + labelToString(new byte[]{0x10, 0x04, 0x04});    //返回打印机状态

        final String[] arr = printTmple.split(" ## ");
        new Thread(new Runnable() {
            @Override
            public void run() {
                for (String str : arr) {
                    if (CHexConver.checkHexStr(str)) {
                        printer.printByte(CHexConver.hexStr2Byte(str));
                    } else if (!EmptyUtil.emptyOfString(str)) {
                        printer.printString(str);
                    }
                }
            }
        }).start();
    }

    public static String labelToString(byte[] bytes) {
        return " ## " + CHexConver.byte2HexStr(bytes) + " ## ";
    }

}
