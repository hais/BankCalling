package pw.hais.bankcalling.utils.print;

import pw.hais.bankcalling.app.HaisApp;

/**
 * 串口打印机
 * Created  on 2016/7/6/006.
 */
public class SerialPortPrinter extends Printer {
    @Override
    public byte[] getInit() {
        return new byte[]{0x1B, 0x40};
    }

    @Override
    public byte[] getBr() {
        return new byte[]{0x0A};
    }

    @Override
    public byte[] getPaper(int x) {
        return new byte[]{0x1B, 0x64, (byte) x};
    }

    @Override
    public byte[] getCutting() {
        return new byte[]{0x1B, 0x6D};
    }

    @Override
    public byte[] getBig_x(int x) {
        return new byte[]{0x1D, 0x21, (byte) x};
    }

    @Override
    public byte[] getLeft() {
        return new byte[]{0x1B, 0x61, 0x00};
    }

    @Override
    public byte[] getCenter() {
        return new byte[]{0x1B, 0x61, 0x01};
    }

    @Override
    public byte[] getRight() {
        return new byte[]{0x1B, 0x61, 0x02};
    }

    @Override
    public void printByte(byte[] bytes) {
        try {
            if (HaisApp.printUtil != null) HaisApp.printUtil.pushMsg(bytes);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void printInt(int[] ints) {
        try {
            if (HaisApp.printUtil != null) HaisApp.printUtil.pushMsg(ints);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void printString(String str) {
        try {
            if (HaisApp.printUtil != null) HaisApp.printUtil.pushMsg(str);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
