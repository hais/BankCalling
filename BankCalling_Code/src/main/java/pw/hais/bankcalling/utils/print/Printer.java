package pw.hais.bankcalling.utils.print;

/**
 * Created  on 2016/7/6/006.
 */
public abstract class Printer {
    protected byte[] init;              //初始化
    protected byte[] br;                //换行
    protected byte[] paper;             //走纸
    protected byte[] cutting;           //切纸
    protected byte[] big_x;               //放大X倍
    protected byte[] left;      //左对齐
    protected byte[] center;    //居中对齐
    protected byte[] right;      //居右对齐

    public abstract byte[] getInit();
    public abstract byte[] getBr();
    public abstract byte[] getPaper(int x);
    public abstract byte[] getCutting();
    public abstract byte[] getBig_x(int x);
    public abstract byte[] getLeft();
    public abstract byte[] getCenter();
    public abstract byte[] getRight();

    public abstract void printByte(byte[] bytes);
    public abstract void printInt(int[] ints);
    public abstract void printString(String str);
}
