package pw.hais.bankcalling.utils;

import java.util.Arrays;

/**
 * Created  on 2016/7/19/019.
 */
public class BusinessUtil {

    //根据 代码和顺序补 0
    public static String getNumberCode(String businessCode, int number) {
        StringBuffer sb = new StringBuffer(businessCode);
        for (int i = (number + "").length(); i < 4; i++) {
            sb.append(0);
        }
        sb.append(number+"");
        return sb.toString();
    }

    //获取 三位数 等待号
    public static String getNumber(int number, int length) {
        StringBuffer sb = new StringBuffer();
        for (int i = (number + "").length(); i < length; i++) {
            sb.append(0);
        }
        sb.append(number);
        return sb.toString();
    }

    public static int[] escape(int[] base) {
        int[] newArray = new int[base.length * 2];
        int index = 0;
        for (int i = 0; i < base.length; i++) {
            if (base[i] == 0x7E) {
                newArray[index++] = 0x7D;
                newArray[index++] = 0x5E;
            } else if (base[i] == 0x7D) {
                newArray[index++] = 0x7D;
                newArray[index++] = 0x5D;
            } else if (base[i] < 0x20 && i != 1) {
                newArray[index++] = 0x7D;
                newArray[index++] = (base[i] ^ 1 << 5);
            } else {
                newArray[index++] = base[i];
            }
        }

        return Arrays.copyOf(newArray, index);
    }

    public static int[] unEscape(byte[] base) {
        int[] newArray = new int[(base.length * 2)];
        int index = 0;
        for (int i = 0; i < base.length; i++) {
            if (base[i] == 0x7D && base[i + 1] == 0x5E) {
                i++;
                newArray[index++] = 0x7E;
            } else if (base[i] == 0x7D && base[i + 1] == 0x5D) {
                i++;
                newArray[index++] = 0x7D;
            } else if (base[i] == 0x7D && base[i + 1] > 0x20) {
                i++;
                newArray[index++] = (base[i] ^ 1 << 5);
            } else {
                newArray[index++] = base[i];
            }
        }

        return Arrays.copyOf(newArray, index);
    }


    public static int getHeight4(int data) {//获取高四位
        int height;
        height = ((data & 0xff00) >> 8);
        return height;
    }

    public static int getLow4(int data) {//获取低四位
        int low;
        low = (data & 0x00ff);
        return low;
    }

    //根据柜台获取 业务。
//    public static Business getDoBusinessMachine(CallingModel.Machine machine) {
//        String[] sort = machine.order.split(",");
//        for (String code : sort) {
//            Business business = getOldMessage(code);
//            if(business!=null){
//                return business;
//            }
//        }
//        return null;
//    }


//    private static Business getOldMessage(String code) {
//        List<Business> list = (List<Business>) DBUtil.findEntityList(Business.class, "code='" + code+"'", "id asc", 0, 1);
//        if (list != null && list.size() > 0) return list.get(list.size() - 1);
//        return null;
//    }
}
