package pw.hais.bankcalling.utils.print;

import android.hardware.usb.UsbDevice;

import com.hs.usbsdk.UsbController;

/**
 * Created  on 2016/11/15/015.
 */

public class USBPrinter extends Printer {
    public static UsbController usb_print;          //USB打印服务
    public static UsbDevice usb_print_dev;          //USB打印服务


    @Override
    public byte[] getInit() {
        return new byte[]{0x1B, 0x40};
    }

    @Override
    public byte[] getBr() {
        return new byte[]{0x0A};
    }

    @Override
    public byte[] getPaper(int x) {
        return new byte[]{0x1B, 0x64, (byte) x};
    }

    @Override
    public byte[] getCutting() {
        return new byte[]{0x1B, 0x6D};
    }

    @Override
    public byte[] getBig_x(int x) {
        return new byte[]{0x1D, 0x21, (byte) x};
    }

    @Override
    public byte[] getLeft() {
        return new byte[]{0x1B, 0x61, 0x00};
    }

    @Override
    public byte[] getCenter() {
        return new byte[]{0x1B, 0x61, 0x01};
    }

    @Override
    public byte[] getRight() {
        return new byte[]{0x1B, 0x61, 0x02};
    }

    @Override
    public void printByte(byte[] bytes) {
        try {
            if (usb_print_dev == null) return;
            usb_print.sendByte(bytes, usb_print_dev);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void printInt(int[] ints) {

    }

    @Override
    public void printString(String str) {
        try {
            if (usb_print_dev == null) return;
            usb_print.sendMsg(str, "GBK", usb_print_dev);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
