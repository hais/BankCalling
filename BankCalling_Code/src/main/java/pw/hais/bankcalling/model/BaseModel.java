package pw.hais.bankcalling.model;

/**
 * Created  on 2016/7/24/024.
 */
public class BaseModel<T> {
    public int code;
    public String msg;
    public String version;
    public T data;
}
