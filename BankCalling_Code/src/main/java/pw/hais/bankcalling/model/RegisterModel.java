package pw.hais.bankcalling.model;

/**
 * Created  on 2016/7/25/025.
 */
public class RegisterModel extends BaseModel<RegisterModel.Register> {
    public String APP_KEY;
    public String channel_no;
    public String DEV_NO;
    public String DEV_NAME;

    public static class Register{
        public String appkey;
        public String time;
    }
}
