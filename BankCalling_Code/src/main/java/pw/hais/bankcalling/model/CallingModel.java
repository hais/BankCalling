package pw.hais.bankcalling.model;

import java.util.List;

import pw.hais.bankcalling.entity.Business;
import pw.hais.bankcalling.entity.Counter;
import pw.hais.bankcalling.entity.Staff;

/**
 * Created  on 2016/7/24/024.
 */
public class CallingModel extends BaseModel<CallingModel.Calling> {

    public static class Calling {
        public String channel_no;       //序号
        public String channel_name;     //机构名称
        public List<Business> business; //业务
        public List<Counter> counters; //柜台
        public List<Staff> staffs;      //员工
        public List<Template> template;         //打印模版
    }

    public static class Template{
        public String Text;
    }
}
