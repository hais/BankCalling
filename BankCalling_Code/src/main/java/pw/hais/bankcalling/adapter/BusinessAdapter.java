package pw.hais.bankcalling.adapter;

import android.widget.TextView;

import java.util.List;

import pw.hais.app.AppAdapter;
import pw.hais.bankcalling.R;
import pw.hais.bankcalling.dao.DateCacheDao;
import pw.hais.bankcalling.entity.Business;
import pw.hais.utils.EmptyUtil;

/**
 * 业务适配器
 * Created  on 2016/7/29/029.
 */
public class BusinessAdapter extends AppAdapter<Business, BusinessAdapter.ViewHolder> {


    public BusinessAdapter(List<Business> mList) {
        super(mList, R.layout.activity_main_item, ViewHolder.class);
    }

    @Override
    public void onBindView(int position, ViewHolder mViewHolder, Business mItem) {
        mViewHolder.text_name.setText(mItem.getName());
        if(EmptyUtil.emptyOfList(mItem.children)){
            mViewHolder.text_message.setText("当前排队人数 " + DateCacheDao.findWaitNumber(mItem.getSchar()) + "人");
        }else{
            mViewHolder.text_message.setText("点击可进入二级菜单");
        }
    }

    public static class ViewHolder extends AppAdapter.ViewHolder {
        public TextView text_name;
        public TextView text_message;
    }

}
