package pw.hais.bankcalling.entity;

/**
 * 机器信息
 * Created  on 2016/7/25/025.
 */
public class Machine {
    public String channel_no;       //渠道编码(机构编码)
    public String mac;              //网卡地址
    public String ip;               //设备IP地址
    public String native_code;      //设备唯一码
    public String dev_name;             //设备名称
    public String dev_no;           //设备编号
    public String dev_type;         //设备类型
    public String appkey;
    public String template;         //打印模版
}
