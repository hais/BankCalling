package pw.hais.bankcalling.entity;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Transient;

import java.util.List;
import org.greenrobot.greendao.annotation.Generated;


/**
 * 业务
 * Created  on 2016/8/4/004.
 */
@Entity
public class Business {
    @Id(autoincrement = true)
    private Long db_id;
    private String id;
    private String color;
    private String icon;
    private String name;
    private String sid;
    private String schar;

    @Transient
    public List<Business> children;
    private int no;          //流水号
    private int call;        //正在服务的窗口
    private int mark;        //分数
    private int status;      //1为正在服务中。、
    private int staffNo;     //员工编号
    private long ticket_time;     //开始取票时间
    private long call_time;     //开始呼叫时间
    private long saccept_time;     //开始办理时间
    private long eaccept_time;     //受理结束时间
    public long getEaccept_time() {
        return this.eaccept_time;
    }
    public void setEaccept_time(long eaccept_time) {
        this.eaccept_time = eaccept_time;
    }
    public long getSaccept_time() {
        return this.saccept_time;
    }
    public void setSaccept_time(long saccept_time) {
        this.saccept_time = saccept_time;
    }
    public long getCall_time() {
        return this.call_time;
    }
    public void setCall_time(long call_time) {
        this.call_time = call_time;
    }
    public long getTicket_time() {
        return this.ticket_time;
    }
    public void setTicket_time(long ticket_time) {
        this.ticket_time = ticket_time;
    }
    public int getStaffNo() {
        return this.staffNo;
    }
    public void setStaffNo(int staffNo) {
        this.staffNo = staffNo;
    }
    public int getStatus() {
        return this.status;
    }
    public void setStatus(int status) {
        this.status = status;
    }
    public int getMark() {
        return this.mark;
    }
    public void setMark(int mark) {
        this.mark = mark;
    }
    public int getCall() {
        return this.call;
    }
    public void setCall(int call) {
        this.call = call;
    }
    public int getNo() {
        return this.no;
    }
    public void setNo(int no) {
        this.no = no;
    }
    public String getSchar() {
        return this.schar;
    }
    public void setSchar(String schar) {
        this.schar = schar;
    }
    public String getSid() {
        return this.sid;
    }
    public void setSid(String sid) {
        this.sid = sid;
    }
    public String getName() {
        return this.name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getIcon() {
        return this.icon;
    }
    public void setIcon(String icon) {
        this.icon = icon;
    }
    public String getColor() {
        return this.color;
    }
    public void setColor(String color) {
        this.color = color;
    }
    public String getId() {
        return this.id;
    }
    public void setId(String id) {
        this.id = id;
    }
    public Long getDb_id() {
        return this.db_id;
    }
    public void setDb_id(Long db_id) {
        this.db_id = db_id;
    }
    @Generated(hash = 1460761363)
    public Business(Long db_id, String id, String color, String icon, String name,
            String sid, String schar, int no, int call, int mark, int status,
            int staffNo, long ticket_time, long call_time, long saccept_time,
            long eaccept_time) {
        this.db_id = db_id;
        this.id = id;
        this.color = color;
        this.icon = icon;
        this.name = name;
        this.sid = sid;
        this.schar = schar;
        this.no = no;
        this.call = call;
        this.mark = mark;
        this.status = status;
        this.staffNo = staffNo;
        this.ticket_time = ticket_time;
        this.call_time = call_time;
        this.saccept_time = saccept_time;
        this.eaccept_time = eaccept_time;
    }
    @Generated(hash = 59677007)
    public Business() {
    }
}
