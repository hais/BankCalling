package pw.hais.bankcalling.entity;

/**
 * Created  on 2015/12/18.
 */
public class EventT {
    public EventType type;
    public Object obj;

    public EventT(EventType type, Object obj) {
        this.type = type;
        this.obj = obj;
    }

    public enum EventType{
        BaseCallCode,   //初始命令
        Print,          //打印机
        Refresh,        //刷新
    }
}
