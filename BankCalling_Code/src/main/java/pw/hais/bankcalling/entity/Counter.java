package pw.hais.bankcalling.entity;

import java.util.List;

/**
 * 柜台
 * Created  on 2016/8/4/004.
 */
public class Counter {
    public String cno;          //窗口编号
    public String led;          //LED屏地址
    public String call;         //物理呼叫器地址
    public boolean enable;      //是否启用该窗口
    public Staff staff;         //登陆的员工
    public int diyWeight;       //自定义权重
    public List<BusinessWeight> regulation; //权重
}
