package pw.hais.bankcalling.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import de.greenrobot.event.EventBus;
import pw.hais.bankcalling.R;
import pw.hais.bankcalling.adapter.BusinessAdapter;
import pw.hais.bankcalling.app.BaseActivity;
import pw.hais.bankcalling.app.HaisApp;
import pw.hais.bankcalling.dao.DateCacheDao;
import pw.hais.bankcalling.entity.Business;
import pw.hais.bankcalling.entity.EventT;
import pw.hais.bankcalling.utils.print.PrintTemp;
import pw.hais.bankcalling.utils.print.SerialPortPrinter;
import pw.hais.bankcalling.utils.print.USBPrinter;
import pw.hais.utils.EmptyUtil;
import pw.hais.utils.L;

/**
 * 主界面
 * Created  on 2016/7/25/025.
 */
public class MainActivity extends BaseActivity implements AdapterView.OnItemClickListener {
    public static final String INTENT_BUSINESS_LIST_GSON = "INTENT_BUSINESS_LIST";
    private ListView listview;
    private BusinessAdapter businessAdapter;
    private boolean isTop = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //获取业务信息
        List<Business> businessList = gson.fromJson(getIntent().getStringExtra(INTENT_BUSINESS_LIST_GSON) + "", new TypeToken<ArrayList<Business>>() {
        }.getType());
        //当前业务分类是否顶级
        if (businessList == null) businessList = DateCacheDao.getBaseBusinesses();
        else isTop = false;

        businessAdapter = new BusinessAdapter(businessList);
        listview = (ListView) findViewById(R.id.listview);
        listview.setAdapter(businessAdapter);
        listview.setOnItemClickListener(this);
    }


    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        if (!EmptyUtil.emptyOfList(businessAdapter.getItem(i).children)) {  //如果有二级菜单则进入二级菜单。
            Intent intent = new Intent(context, MainActivity.class);
            intent.putExtra(INTENT_BUSINESS_LIST_GSON, gson.toJson(businessAdapter.getItem(i).children));
            startActivity(intent);
        } else {    //开始 打票业务
            Business business = businessAdapter.getItem(i);
            business.setDb_id(null);
            business.setTicket_time(new Date().getTime());
            business.setStatus(1);
            business.setNo(DateCacheDao.findNewBusinessNoBySchar(business.getSchar()) + 1);
//            DBUtil.save(business);  //业务状态存到数据库
            L.e("保存：" + gson.toJson(business));
            HaisApp.daoSession.getBusinessDao().save(business);
            //发送指定 刷新界面当前等待人数
            EventBus.getDefault().post(new EventT(EventT.EventType.Refresh, 0));
            L.showShort("小票正在打印中...");
            PrintTemp.printMessage(new SerialPortPrinter(), business,HaisApp.machine.template);
            if(!isTop)finish();
        }
    }

    @Override
    public void onEventMainThread(EventT eventEntity) {
        super.onEventMainThread(eventEntity);
        switch (eventEntity.type) {
            case Refresh:
                if (businessAdapter != null) {
                    L.e("刷新界面-.-");
                    businessAdapter.notifyDataSetChanged();
                }
                break;
        }
    }
}
