package pw.hais.bankcalling.app;


/**
 * Created  on 2016/7/22/022.
 */
public class V_HTTP {
    public static final String HTTP_SERVER = "http://192.168.10.56:8080/";
//    public static final String HTTP_SERVER = "http://weixin.tonsincs.com/";

    public static final String INIT_MACHINE = HTTP_SERVER + "kuaiyou/api/init.do";             //初始化
    public static final String REGISTER_MACHINE = HTTP_SERVER + "kuaiyou/api/register.do";     //设备注册

    public static final String UPLOAD_MACHINE = HTTP_SERVER + "kuaiyou/api/upload.do";         //上傳結果
}
