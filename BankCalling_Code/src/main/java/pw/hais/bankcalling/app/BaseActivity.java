package pw.hais.bankcalling.app;

import android.os.Bundle;

import de.greenrobot.event.EventBus;
import pw.hais.app.AppBaseActivity;
import pw.hais.bankcalling.entity.EventT;
import pw.hais.utils.L;

/**
 * Activity基础类
 * Created  on 2016/7/22/022.
 */
public class BaseActivity extends AppBaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
    }

    @Override
    protected void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
        EventBus.getDefault().post(new EventT(EventT.EventType.Refresh, 0));
    }

    @Override
    protected void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }

    //订阅事件
    public void onEventMainThread(EventT eventEntity) {
        L.i(tag + "-收到事件：" + eventEntity.type);
    }
}
