package pw.hais.bankcalling.service;

import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.Nullable;

import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.List;

import android_serialport_api.CHexConver;
import pw.hais.bankcalling.dao.CallDao;
import pw.hais.bankcalling.entity.Counter;
import pw.hais.bankcalling.utils.print.SerialPortPrinter;
import pw.hais.bankcalling.utils.print.USBPrinter;
import pw.hais.utils.SPUtil;

import static pw.hais.bankcalling.app.V.SP_COUNTERS_LIST;

/**
 * Created  on 2016/7/28/028.
 */
public class CallService extends Service {
    List<Counter> counterList;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        serialPortPrinter = new SerialPortPrinter();
        counterList = SPUtil.getObject(SP_COUNTERS_LIST, new TypeToken<ArrayList<Counter>>() {
        }.getType());
        lookSendMsgToCall();
        lookSendMsgToPrint();
    }

    private void lookSendMsgToCall() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                CallDao.sendWaitNumber(counterList);
                lookSendMsgToCall();
            }
        }, 100);
    }

    private SerialPortPrinter serialPortPrinter;
    private void lookSendMsgToPrint() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() { //每10秒查询打印机状态
                serialPortPrinter.printByte(new byte[]{0x10, 0x04, 0x04});
                lookSendMsgToPrint();
            }
        }, 10 * 1000);
    }


}
