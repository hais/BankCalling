package pw.hais.bankcalling.service;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import android_serialport_api.CHexConver;
import de.greenrobot.event.EventBus;
import pw.hais.app.UtilConfig;
import pw.hais.bankcalling.app.HaisApp;
import pw.hais.bankcalling.dao.BankCallingDao;
import pw.hais.bankcalling.dao.CallDao;
import pw.hais.bankcalling.dao.DateCacheDao;
import pw.hais.bankcalling.dao.LEDDao;
import pw.hais.bankcalling.entity.Business;
import pw.hais.bankcalling.entity.EventCall;
import pw.hais.bankcalling.entity.EventT;
import pw.hais.bankcalling.utils.BusinessUtil;
import pw.hais.bankcalling.utils.CallUtil;
import pw.hais.utils.L;

/**
 * 终端事件处理
 * Created  on 2016/8/1/001.
 */
public class EventService extends Service {

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        EventBus.getDefault().register(this);
    }

    //订阅事件
    public void onEventMainThread(EventT eventEntity) {
        L.i("EventService-收到事件：" + eventEntity.type);
        switch (eventEntity.type) {
            case BaseCallCode:
                int[] bytes = BusinessUtil.unEscape((byte[]) eventEntity.obj);
                analyticMessageAndSend(bytes);
                break;
            case Print:
                String str = CHexConver.byte2HexStr((byte[]) eventEntity.obj);
                L.e("打印机返回状态：" + str);
                if ("0C".equals(str)) {

                } else if ("6C".equals(str)) {
                    L.showLong("打印机纸张已耗尽，请联系管理员增加纸张！");
                }
                break;
        }
    }

    private void analyticMessageAndSend(int[] bytes) {
        Business business;
        EventCall eventCall = new EventCall();
        eventCall.call = bytes[1];
        eventCall.type = bytes[3];
        eventCall.content = Arrays.copyOfRange(bytes, 4, bytes.length - 3);
        L.e("收到：" + Integer.toHexString(eventCall.call) + "-" + Integer.toHexString(eventCall.type) + "-" + CHexConver.showByte(eventCall.content));
        switch (eventCall.type) {
            case 0x55:  //U，要求派号
                List<Business> list = HaisApp.daoSession.getBusinessDao().loadAll();
                L.e("--"+ UtilConfig.GSON.toJson(list));
                business = DateCacheDao.findBusinessByCon(eventCall.call, 1);
                if (business == null){
                    LEDDao.sendMessageToLed(eventCall.call, eventCall.call + " 欢迎光临");
                    return;
                }
                CallDao.sendNumberClient(eventCall.call, business.getSchar(), business.getNo());
                business.setStatus(2);
                business.setCall(eventCall.call);
                business.setCall_time(new Date().getTime());
                HaisApp.daoSession.getBusinessDao().update(business);
                EventBus.getDefault().post(new EventT(EventT.EventType.Refresh, 0));
                break;
            case 0x57:  //W，正在等待nnnn号顾客

                break;
            case 0x53:  //S，正在服务nnnn号顾客
                business = DateCacheDao.findBusinessByCon(eventCall.call, 2);
                if (business != null) return;
                business.setStatus(3);
                business.setSaccept_time(new Date().getTime());
                HaisApp.daoSession.getBusinessDao().update(business);
                break;
            case 0x50:  //P，呼叫nnnn号顾客
                CallDao.replyNumberToClient(eventCall.call);  //发送说明指令已被接受
                LEDDao.sendMessageToLed(eventCall.call, "　请" + CallUtil.asciiToString(eventCall.content) + "到" + eventCall.call + "号窗口办理业务　");
                HaisApp.audioUtil.speech("请 " + CallUtil.asciiToString(eventCall.content) + " 到" + eventCall.call + "号窗口办理业务", eventCall.call + "");
                break;
            case 0x47:  //G，把nnnn号转义到www号窗口，如果WWW的第一位是字母则转移到队列，后两个WW无效。
                break;
            case 0x45:  //E，暂停服务
                LEDDao.sendMessageToLed(eventCall.call, "　暂停服务　");
                break;
            case 0x49:  //I，求助
                break;
            case 0x4A:  //J，服务开始
                LEDDao.sendMessageToPingjia(eventCall.call, 1);  //发送评价
                LEDDao.sendMessageToLed(eventCall.call, "正在服务" + CallUtil.asciiToString(eventCall.content) + "　");
                break;
            case 0x4B:  //K，服务结束
            case 0x4C:  //L,Clear键
                LEDDao.sendMessageToLed(eventCall.call, eventCall.call + " 欢迎光临");

                business = DateCacheDao.findBusinessByCon(eventCall.call, 2);
                if (business == null) return;
                LEDDao.sendMessageToPingjia(eventCall.call, 2);  //发送请评价
                business.setStatus(0);
                business.setSaccept_time(new Date().getTime());
                HaisApp.daoSession.getBusinessDao().update(business);
                BankCallingDao.updateDate(business, null);
                break;
            case 0x59:  //Y，评分按键，n为键值(Y+n)
                business = DateCacheDao.findBusinessByCon(eventCall.call, 3);
                if (business == null) return;
                business.setStatus(4);
                business.setMark(Integer.parseInt(CallUtil.asciiToString(eventCall.content)));
                business.setEaccept_time(new Date().getTime());
                HaisApp.daoSession.getBusinessDao().update(business);
                BankCallingDao.updateDate(business, null);
                break;
            case 0x51:  //Q，登录系统，nnnnnn\xxxxxx n是工号，x是密码 ，\是分割，单独Q为退出。
                boolean isSuccess = DateCacheDao.staffLogin(eventCall.call, CallUtil.asciiToString(eventCall.content));
                if (isSuccess) {
                    CallDao.replyLoginToClient(eventCall.call, eventCall.content);
                } else {
                    CallDao.replyLoginToClient(eventCall.call, null);
                }
                break;
            case 0x4D:  //M，设置主机系统日期时间。
                break;
        }
    }
}
