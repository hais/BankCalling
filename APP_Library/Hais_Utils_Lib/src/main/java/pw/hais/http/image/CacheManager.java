package pw.hais.http.image;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Environment;

import pw.hais.app.UtilConfig;
import pw.hais.http.base.BaseHttp;
import pw.hais.utils.L;

/**
 * Created by Hais1992 on 2015/8/27.
 */
public class CacheManager {
    public static final int default_drawable_id = UtilConfig.DEFAULT_DRAWABLE_ID;   //默认图片
    public static final int error_drawable_id = UtilConfig.ERROR_DRAWABLE_ID;           //错误图片
    public static final int PNG_COMPRESS = UtilConfig.PNG_COMPRESS;// 存入SD卡时，PNG的压缩率
    public static final int JPG_COMPRESS = UtilConfig.JPG_COMPRESS;// 存入SD卡时，JPG的压缩率
    public static Context context = UtilConfig.CONTEXT;                 //上下文

    public static final Bitmap.Config BITMAP_CONFIG = Bitmap.Config.RGB_565;    //压缩编码
    public static final boolean SD_IS_WIRTE = Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED);  //判断sd卡是否存在
    public static String CACHE_DIR = getCacheFilePath();  //文件缓存目录
    private static SwapCache bitmapCache = new SwapCache();             //内存缓存

    /**
     * 存入缓存
     *
     * @param url     key
     * @param bitmap
     * @param isCover 是否覆盖
     */
    public static void putBitmapCache(String url, Bitmap bitmap, boolean isCover) {
        bitmapCache.putBitmap(url, bitmap, isCover);
    }


    /**
     * URL 获取缓存图片 Bitmap
     * <p/>
     * maxWidth、maxHeight 为 -1的时候读取原图
     * maxWidth、maxHeight 为 0 的时候自动压缩
     *
     * @param url
     * @return
     */
    public static Bitmap getBitmapCache(String url, int maxWidth, int maxHeight) {
        Bitmap bitmap = bitmapCache.getBitmap(url);     //从内存获取
        if (bitmap == null) { //从SD卡获取
            bitmap = SDCardCacheManager.getImageFromSDCard(url, maxWidth, maxHeight);
            if (bitmap != null) {   //如果内存卡有缓存，就存入 内存
                putBitmapCache(url, bitmap, false);
            }
        }
        return bitmap;
    }

    /**
     * URL 获取缓存图片 Bitmap
     * <p/>
     * maxWidth、maxHeight 为 -1的时候读取原图
     * maxWidth、maxHeight 为 0 的时候自动压缩
     *
     * @param
     * @return
     */
    public static Bitmap getBitmapCache(Uri uri, int maxWidth, int maxHeight) {
        Bitmap bitmap = bitmapCache.getBitmap(uri.toString());     //从内存获取
        if (bitmap == null) { //从SD卡获取
            bitmap = SDCardCacheManager.getImageFromSDCard(uri, maxWidth, maxHeight);
            if (bitmap != null) {   //如果内存卡有缓存，就存入 内存
                putBitmapCache(uri.toString(), bitmap, false);
            }
        }
        return bitmap;
    }


    /**
     * 根据url生成文件名
     */
    public static String getUrlToFileName(String url) {
        return url.replace("http", "").replace(":", "").replace("/", "");
    }

    /**
     * 根据url 获取缓存地址，带路径
     */
    public static String getUrlToLocalURL(String url) {
        return CACHE_DIR + url.replace("http", "").replace(":", "").replace("/", "");
    }

    /**
     * 获取 文件 缓存路径
     */
    public static String getCacheFilePath() {
        String path = "";
        try {
            //读取 配置文件，获取路径文件夹
            if("".equals(UtilConfig.IMAGE_CACHE_DIR)){
                path = CacheManager.context.getExternalCacheDir().getAbsolutePath() + "/";
            }else if(CacheManager.SD_IS_WIRTE){
                if("".equals(UtilConfig.IMAGE_CACHE_DIR)){
                    path = Environment.getExternalStorageDirectory().toString() + "/Android/data/Hais_Utils_Cache/";//获取跟目录
                }else{
                    path = Environment.getExternalStorageDirectory().toString() + UtilConfig.IMAGE_CACHE_DIR;//获取跟目录
                }
            }else{
                path = CacheManager.context.getCacheDir().getAbsolutePath() + "/" + UtilConfig.IMAGE_CACHE_DIR;  //获取缓存目录
            }
        } catch (Exception e) {   //如果木有 读写权限，则把东西写入 缓存
            path = CacheManager.context.getCacheDir().getAbsolutePath() + "/" + UtilConfig.IMAGE_CACHE_DIR;  //获取缓存目录
            if(path.indexOf("/data/data/") != -1 && CacheManager.SD_IS_WIRTE){
                path = Environment.getExternalStorageDirectory().toString() + "/Android/data/Hais_Utils_Cache/";//获取跟目录
            }
        }

        L.i(BaseHttp.TAG, "SD卡：" + path);
        return path;
    }

}
