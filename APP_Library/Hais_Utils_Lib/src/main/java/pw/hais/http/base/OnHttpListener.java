package pw.hais.http.base;

import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

/**
 * Http请求回调
 * Created by Hais1992 on 2015/8/25.
 */
public abstract class OnHttpListener<T> {

    public abstract void onSuccess(Response response, T data);


    public void onError(Request request, Exception e) {
        e.printStackTrace();
    }


    public void onString(Response response, String data) {
    }

    public void onProgress(long countLength,int progress) {

    }

    //不管成功失败都回掉
    public void onHttpEnd(boolean isTrue) {

    }

}
