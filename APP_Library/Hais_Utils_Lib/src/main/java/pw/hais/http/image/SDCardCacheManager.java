package pw.hais.http.image;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import pw.hais.http.base.BaseHttp;
import pw.hais.utils.L;
import pw.hais.utils.ScreenUtil;

/**
 * SD 卡 缓存管理
 * Created by Hais1992 on 2015/12/31.
 */
public class SDCardCacheManager {

    /**
     * 根据URL 从SD卡读取 缓存 图片
     *
     * @param url       地址
     * @param maxWidth  宽度，0为自适应
     * @param maxHeight 高  0
     * @return
     */
    public static Bitmap getImageFromSDCard(String url, int maxWidth, int maxHeight) {
        if (!CacheManager.SD_IS_WIRTE) return null;
        String filename = CacheManager.getUrlToFileName(url);    //根据url生成文件名
        String path = CacheManager.CACHE_DIR + "/" + filename;  // 获取应用图片缓存路径
        try {
            if (new File(path).exists()) {
                return decodeBitmapFile(path, maxWidth, maxHeight);
            }
        } catch (Exception e) {
            L.i(BaseHttp.TAG, "读取" + path + "图片出错！");
        }
        return null;
    }

    /**
     * 根据 Uri 从SD卡读取 缓存 图片
     *
     * @param Uri       地址
     * @param maxWidth  宽度，0为自适应
     * @param maxHeight 高  0
     * @return
     */
    public static Bitmap getImageFromSDCard(Uri uri, int maxWidth, int maxHeight) {
        if (!CacheManager.SD_IS_WIRTE) return null;
        String path = uri.getPath();  // 获取应用图片缓存路径
        try {
            if (new File(path).exists()) {
                return decodeBitmapFile(path, maxWidth, maxHeight);
            }
        } catch (Exception e) {
            L.i(BaseHttp.TAG, "读取" + path + "图片出错！");
        }
        return null;
    }

    /**
     * 将bitmap存储到SD卡-覆盖
     */
    public static void saveBmpToSd(Bitmap bitmap,boolean isCover, String url) {
        if (bitmap == null || !CacheManager.SD_IS_WIRTE) return;

        String filename = CacheManager.getUrlToFileName(url);// 获取文件名
        String dir = CacheManager.CACHE_DIR;   // 获取存放目录
        File dirFile = new File(dir);
        File file = new File(dir, filename);
        try {
            if (!dirFile.exists()) dirFile.mkdirs();    //目录不存在，则创建
            if (file.exists() && !isCover) return;    //文件存在则结束

            file.createNewFile();    //创建文件
            //保存 图片 到 文件
            BufferedOutputStream outStream = new BufferedOutputStream(new FileOutputStream(file));

            /* 720P以下手机内存小，固压缩比例增大防止OOM */
            if (url.indexOf(".png") >= 0) {
                bitmap.compress(Bitmap.CompressFormat.PNG, CacheManager.PNG_COMPRESS, outStream);
            } else {
                bitmap.compress(Bitmap.CompressFormat.JPEG, CacheManager.PNG_COMPRESS, outStream);
            }

            outStream.flush();
            outStream.close();
            L.i(BaseHttp.TAG, "已存SD:" + dir + filename);
        } catch (FileNotFoundException e) {
            L.i(BaseHttp.TAG, "图片文件写入SD出错" + filename);
        } catch (IOException e) {
            L.i(BaseHttp.TAG, "图片写入处理除错" + filename);
            e.printStackTrace();
        }
    }

    /**
     * 清空本地缓存
     */
    public static boolean cleanLocalCache() {
        File dir = new File(CacheManager.CACHE_DIR);
        boolean b = deleteDir(dir);
        File file = new File(CacheManager.CACHE_DIR);
        if (!file.exists()) {
            file.mkdirs();
        }
        return b;
    }

    /**
     * 递归删除文件
     */
    private static boolean deleteDir(File dir) {
        if (dir.isDirectory()) {
            String[] children = dir.list();
            // 递归删除目录中的子目录下
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
        }
        // 目录此时为空，可以删除
        return dir.delete();
    }

    /**
     * 读取文件 解码 成 Bitmap
     * maxWidth、maxHeight 为 -1的时候读取原图
     * maxWidth、maxHeight 为 0 的时候自动压缩
     */
    public static Bitmap decodeBitmapFile(String path, int maxWidth, int maxHeight) {
        BitmapFactory.Options opt = new BitmapFactory.Options();

        opt.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path, opt);
        if (maxWidth > 0 && maxWidth > 0) {
            opt.inSampleSize = computeSampleSize(opt, -1, maxWidth * maxHeight);
        } else if(maxWidth == 0 && maxWidth==0){
            opt.inSampleSize = computeSampleSize(opt, -1, (ScreenUtil.getScreenWidth() * 2 / 3) * (ScreenUtil.getScreenHeight() * 2 / 3));
        }else{
            opt.inSampleSize = computeSampleSize(opt, -1, -1);
        }

        opt.inPreferredConfig = CacheManager.BITMAP_CONFIG;
        opt.inPurgeable = true;
        opt.inInputShareable = true;
        opt.inTempStorage = new byte[8 * 1024];
        opt.inJustDecodeBounds = false;
        L.i(BaseHttp.TAG, "读取图片：" + path);
        return BitmapFactory.decodeFile(path, opt);
    }

    /**
     * 计算压缩率
     *
     * @param options
     * @param minSideLength
     * @param maxNumOfPixels
     * @return
     */
    public static int computeSampleSize(BitmapFactory.Options options, int minSideLength, int maxNumOfPixels) {
        int initialSize = computeInitialSampleSize(options, minSideLength, maxNumOfPixels);

        int roundedSize;
        if (initialSize <= 8) {
            roundedSize = 1;
            while (roundedSize < initialSize) {
                roundedSize <<= 1;
            }
        } else {
            roundedSize = (initialSize + 7) / 8 * 8;
        }
        return roundedSize;
    }

    public static int computeInitialSampleSize(BitmapFactory.Options options, int minSideLength, int maxNumOfPixels) {
        double w = options.outWidth;
        double h = options.outHeight;
        int lowerBound = (maxNumOfPixels == -1) ? 1 : (int) Math.ceil(Math.sqrt(w * h / maxNumOfPixels));
        int upperBound = (minSideLength == -1) ? 128 : (int) Math.min(Math.floor(w / minSideLength), Math.floor(h / minSideLength));

        if (upperBound < lowerBound) {
            // return the larger one when there is no overlapping zone.
            return lowerBound;
        }

        if ((maxNumOfPixels == -1) && (minSideLength == -1)) {
            return 1;
        } else if (minSideLength == -1) {
            return lowerBound;
        } else {
            return upperBound;
        }
    }
}
