package pw.hais.app;

import android.app.Activity;
import android.app.Application;

import java.util.LinkedList;

import pw.hais.utils.CrashUtil;

/**
 * APP 全局
 * Created by hais1992 on 15-5-13.
 */
public class AppApplication extends Application implements CrashUtil.LogErrorrListener{
    public static LinkedList<Activity> activities = new LinkedList<Activity>();
    public static AppApplication app;

    @Override
    public void onCreate() {
        super.onCreate();

        app = this;

        //初始化工具类
        UtilConfig.init(getBaseContext());
//        CrashUtil.getInstance().setListener(this);  //设置日记监听
    }

    public void addActivity(Activity activity) {
        if (!activities.contains(activity)) {
            activities.add(activity);
        }
    }


    /**
     * 移除Activity引用
     */
    public void removeActivity(Activity activity) {
        activities.remove(activity);
    }

    /**
     * 退出APP
     */
    public static void exitApp() {
        try {
            if (activities != null && !activities.isEmpty()) {
                for (Activity a : activities) {
                    a.finish();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.exit(0);
        android.os.Process.killProcess(android.os.Process.myPid());
    }

    @Override
    public void onErrorListener(String log){}
}
