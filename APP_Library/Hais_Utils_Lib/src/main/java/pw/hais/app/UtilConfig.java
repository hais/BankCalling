package pw.hais.app;

import android.content.Context;

import com.google.gson.Gson;

import pw.hais.sqlite.DBUtil;
import pw.hais.utils.AudioHandleUtils;
import pw.hais.utils.CrashUtil;
import pw.hais.utils.L;
import pw.hais.utils.R;


/**
 * 工具配置类
 * Created by Hais1992 on 2015/3/26.
 */
public class UtilConfig {
    //全局
    public static boolean DEBUG = true;                 //是否开启调试
    public static final String CHARSET = "UTF-8";          //编码
    public static Context CONTEXT = null;                 //上下文
    public static String APP_ID = "pw.hais.util";      //包名
    public static final Gson GSON = new Gson();           //全局Gson

    //日记
    public static boolean LOG_PRINTF = UtilConfig.DEBUG;  //是否开启日记输出
    public static String LOG_DEFAULL_TAG = "Log";    //日记缺省标签
    public static String LOG_PREFIX = "-TBQ";            //日记输出前缀

    //数据库
    public static String DB_NAME = UtilConfig.APP_ID + ".db";      //数据库名
    public static int DB_VERSION = 1;                  //数据库版本

    //HTTP、图片
    public static final int HTTP_ConnectTimeout = 1000;     //请求网络数据 连接超时时间
    public static final int HTTP_WriteTimeout = 1000;       //请求网络数据 请求超时时间
    public static final int HTTP_ReadTimeout = 1000;        //请求网络数据 读取超时时间

    public static final int DEFAULT_DRAWABLE_ID = R.drawable.image_default;          //默认图片
    public static final int ERROR_DRAWABLE_ID = R.drawable.image_error;              //错误图片
    public static final int PNG_COMPRESS = 100;         //图片缓存时的 PNG 压缩率,
    public static final int JPG_COMPRESS = 100;         //图片缓存时的 JPG 压缩率,
    public static final String IMAGE_CACHE_DIR = "";    //缓存路径 如果为 "" 则缓存到 数据文件夹,建议为空


    /**
     * 初始化 Hais 工具类
     *
     * @param context 上下文
     */
    public static void init(Context context) {
        UtilConfig.CONTEXT = context;
//        DBUtil.init(context);
        L.i("Hais-Util", "Hais-Util初始化成功！");

        //日记捕抓
//        CrashUtil.getInstance().init(AppApplication.app.getApplicationContext());
        //初始化音频播放工具
        AudioHandleUtils.getInstance().initConfig(AppApplication.app.getBaseContext());
    }

}
