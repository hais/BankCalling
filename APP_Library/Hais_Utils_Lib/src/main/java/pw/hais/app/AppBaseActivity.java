package pw.hais.app;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.google.gson.Gson;

import pw.hais.ui.LoadProgressDialog;


/**
 * 基础Activity
 * Created by hais1992 on 15-5-6.
 */
public class AppBaseActivity extends AppCompatActivity implements LoadProgressDialog.LoadDialog {
    protected String tag = "";              //当前TAG
    protected AppBaseActivity context;              //方便设置监听器等。。
    protected Gson gson = UtilConfig.GSON;
    protected int V_ID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        tag = getClass().getSimpleName();   //获取 当前类名，方便 打log

        context = this;
        ((AppApplication) getApplicationContext()).addActivity(this);
    }


    @Override
    public void setContentView(int layoutResID) {
        super.setContentView(layoutResID);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    public void finish() {
        super.finish();
        ((AppApplication) getApplicationContext()).removeActivity(this);
    }

    private boolean isDrawFinish = false;

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {

        if (hasFocus && !isDrawFinish) {
            isDrawFinish = true;
            onDrawFinish();
        }
    }

    /**
     * 渲染完毕
     */
    public void onDrawFinish() {

    }


    /*----------------------------加载弹出窗------------------------------------*/
    private LoadProgressDialog loadDialog;    //菊花

    @Override
    public void loadDialogShow(String text) {
        if (loadDialog == null) loadDialog = new LoadProgressDialog(this);
        loadDialog.show(text);
    }

    @Override
    public void loadDialogDismiss() {
        if (loadDialog != null) loadDialog.dismiss();
    }
}
