package pw.hais.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.ImageButton;

import pw.hais.helper.TintedBitmapDrawable;
import pw.hais.utils.ImageUtil;
import pw.hais.utils.R;

/**
 * 带选中，和点击效果 的  ImageView
 * Created by hais1992 on 15-5-6.
 */
public class ImageFocusView extends ImageButton {
    private boolean select_state = false;    //选中状态
    private int focusColor;

    public ImageFocusView(Context context) {
        super(context);
        init(context, null);
    }

    public ImageFocusView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public ImageFocusView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    private BitmapDrawable focusDrawable;
    private Bitmap defaultBitmap;
    private Drawable defaultDrawable;

    private void init(Context context, AttributeSet attrs) {
        setBackgroundColor(0x00000000);
        TypedArray array = context.obtainStyledAttributes(attrs, R.styleable.FocusView);
        focusColor = array.getColor(R.styleable.FocusView_focus_color, 0xff9e9e9e);

        defaultDrawable = getDrawable();  //获取当前背景图Drawable
        defaultBitmap = ImageUtil.drawableToBitmap(defaultDrawable);  //获取当前背景图Bitmap
        this.setDrawingCacheEnabled(true);
        this.setClickable(true);
    }


    @Override
    protected void dispatchDraw(Canvas canvas) {
        super.dispatchDraw(canvas);

        if (!isEnabled()) return;

        if (select_state) {
            if (focusDrawable == null)
                focusDrawable = new TintedBitmapDrawable(getResources(), defaultBitmap, focusColor);
            setImageDrawable(focusDrawable);
        } else {
            setImageDrawable(defaultDrawable);
        }

    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                select_state = true;        //设置当前需求为 选中 状态
                invalidate();                   //重新绘图
                break;
            case MotionEvent.ACTION_UP:
            case MotionEvent.ACTION_CANCEL:
                select_state = false;       //设置当前需求为 未选中 状态
                invalidate();               //重新绘图
                break;
        }
        return super.onTouchEvent(event);
    }

}
