package android_serialport_api;

/**
 * 设备实体类
 * Created by Administrator on 2015/8/13.
 */
public class DevicesInfo {
    public String name;
    public String path;

    public DevicesInfo(String name, String path) {
        this.name = name;
        this.path = path;
    }
}
